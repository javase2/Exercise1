package QDTS;

public class DeveloperChoice {

    public static void main(String[] args) throws Exception {
        // A variable to keep the user's choice
        char yourChoice;

        // The choices available
        System.out.println("What is your preference for Java development?");
        System.out.println("1 - Eclipse");
        System.out.println("2 - Rational Application Developer");

        System.out.print("Type your selection (1 or 2) here: ");
        yourChoice = (char) System.in.read();

        if (yourChoice == '1')
            System.out.println("Good choice! You are going to use Eclipse");
        else if (yourChoice == '2')
            System.out.println("Good choice! You are going to use Rational");
        else
            System.out.println("Invalid selection. Re-run the program");
    }
}

