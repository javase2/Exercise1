package QDTS;

import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for DeveloperChoice.
 */
@DisplayName("First exercise Test")
public class DeveloperChoiceTest {
    private final static PipedOutputStream pipe = new PipedOutputStream();
    private final static ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final static ByteArrayOutputStream err = new ByteArrayOutputStream();

    private final static InputStream originalIn = System.in;
    private final static PrintStream originalOut = System.out;
    private final static PrintStream originalErr = System.err;

    @BeforeAll
    public static void catchIO() throws IOException {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
        System.setIn(new PipedInputStream(pipe));
    }

    @AfterAll
    public static void releaseIO() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setIn(originalIn);
    }


    @Test
    @DisplayName("First exercise Test")
    public void inreadTest() throws Exception {

        char yourChoice = 0;
        PrintWriter in = new PrintWriter(new OutputStreamWriter(pipe), true);
        in.println(yourChoice);
        DeveloperChoice.main(new String[0]);
        String[] output = out.toString().split(System.lineSeparator());
        assertEquals("What is your preference for Java development?", output[0]);
        assertEquals("1 - Eclipse", output[1]);
        assertEquals("2 - Rational Application Developer", output[2]);
        assertEquals("Type your selection (1 or 2) here: Invalid selection. Re-run the program", output[3]);
    }
}