# Exercise1
Компиляция и запуск Java приложений

Код для выполнения 2 раздела из упражнения 1:

Скопируйте указанный ниже код
Вставьте с ПОЛНОЙ заменой в файл tasks.json
Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

После копирования можно приступать к выполнению 5-го пункта

Код:

{
    "tasks": [
        {
            "type": "che",
            "label": "DeveloperChoice build",
            "command": "mvn clean install",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise1",
                "component": "maven"
            },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "DeveloperChoice build and run",
            "command": "mvn clean install && java -jar ./target/*.jar",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise1",
                "component": "maven"
             },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "DeveloperChoice test",
            "command": "mvn verify",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise1",
                "component": "maven"
             },
            "problemMatcher": []
        }
    ]
}
